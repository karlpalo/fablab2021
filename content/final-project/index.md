+++
title = "Final Project"
description = "This is the final project page"
+++

This is the idea for my final project:

An GPS-tracker for tracking the movement of floating algae!

After collaborating with biologists I have found that there is a demand for a cheap (~50€) Device that could be attached to floating algae and then could be tracked via GPS.

The algae in question is called fucus vesiculous and exists in free-floating spheres that vary in size from tiny ones to cauliflower-sized ones.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="fucus.jpg" alt="small algae">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="fucus2.jpg" alt="large algae">}}{{</image>}}
  </div>
</div>


This type of devices should:
- be easy to manufacture (as 10-100 pcs would be needed for a reliable study.)
- Have a relatively long lifetime (minimum several months)
- Tolerate harsh conditions (seawater + freezing)
- Be able to record the location on a daily basis
- Be able to transmit the location intermittently, even from remote locations
- Be light enough to be attached to, and float with the algae without disturbing the natural movement too much
- GPS-signal does not propagate underwater --> the device (or at least the transmitter/reciever) MUST float (a buoy wouldbe simplest, perhaps a time triggered flotation device would be possible (but complicated)?)
- Be small enough to be attached to the algae in a reasonable manner.

For my initial design I just drew a rough image of how the device could look like when it is attached to algae.

{{< image src="initial sketch.jpg" alt="picture of my initial design">}}{{</image>}}

Some design criteria are apparent from the image. First of all the device must have neutral buoyancy during operation and positive buoyancy during recovery. This issue is solved by 2-part design where the "Attachement to algae"-part is heavier than water and the "Detaching part" will float on the top of the water.

Furthermore the detaching part will need to be designed so internally that the sensors/antennas remain over water surface level.


## Application choices coming from design parameters

**All Components**
- Minimize price
- Minimize excess power consumption
- Design the system to be easily replicable


**Shell:**
- Weight might be an issue, but perhaps can be countered with well planned buoyancy properties? (larger weight allows for larger battery)
- Needs to be attachable to floating bladderwrack in a way to avoid detaching, even in storm conditions
- Biological inertness preferrable.
- Full sealing from water and enviromental hazards, winter survival would be great, but not absolutely vital. (Perhaps a freeze-test for the shell?)
- Materials must withstand saltwater and UV-radiation.
- Able to orient itself to make the solar panels face upwards.
- Contains a note for the finder (e.g. deliver this to x zoological centre plz)

**GPS:**
- Should consume only small amount of electricity
- Activation only 1-4 times daily to conserve power, (Perhaps taking an average of N-measurements, or logging n measurements to ensure accuracy)
- Recorded data should be stored until transmission becomes possible. --> External memory unit?
- Should be also able to increase signal sampling rate, for potential pickup. (If the device is being picked up, power saving is not a priority)

**Connection to the outside world. (Radio+satellite or gsm?)**
**Radio+satellite:**
- Able to transmit/recieve all over the world
- Connection costs might be significant
- Aproppriate transmitters and signal interpreters might be proprietary tech or require specialised circuit boards?
- Would make sense to have regular, small transmissions whenever the satellite is passing over
- This tech is already being used to track animals on a global scale

**Radio + Radio Reciever**
- Perhaps shortwave-radio could work in the baltic sea area (also take note of regulations as radio traffic is somehwhat regulated)
- Amateur radio channels probably the best bet. (But possible interception of signals by eager enthusiasts)
- 2-directional communication would be needed. (e.g. in addition to the actual device, a communicator capable of data communication needs to be built/bought)
- How this would work with the swarm-approach (overlapping signals?), assigning each "unit" a different "start transmission-code?"

**GSM:**
- Able to transmit/receive only near coasts (e.g. where the GSM-network is) --> Needs a control system to see if it is on the open sea or near coasts
- If it goes to open sea it is lost (unless you get very lucky with a boat with a portable mobile mast)
- However, with 3G or 4G, the transferred data amounts can be large
- Text messages might also be option? (What is the most reliable way of transfer?)
- Perhaps the easiest option for a prototype. (Takes advantage of the existing infrastructures)
- Requires a Pre-paid card.
- Relatively easy to swarm?

**Both:**
- Added antennas for improving transmission/receiving quality? (I need help in antenna design!)
- Antenna might be vital to ensure signal propagation (Saltwater acts as an effective barrier)

**Power system:**
- Principle: minimize power consumption when possible
- Solar panels and a rechargeable battery might be best. (Allows for the potential of lost units to be "found again")
- Failsafes and controls for problems and instabilities resulting from low power level
- Tracking of battery level would be nice (especially as a trigger for failsafes)


**Solar Panel**
- A very basic one should do

**Internal clock**
Essential especially for reboots and location tracking after power loss (unless data is uploaded to satellites regularly), E.g. for the GSM-option



## First milestone: Meeting of Biologists on 5.2.2012
Questions to be asked:
- What have been the main limitations with current tech (most likely the price, as GPS-trackers go from 1-5 k€ per piece)
- What is their opinion on the GSM vs Satellite ([ARCOS](https://www.cls-telemetry.com/argos-solutions/argos-services/)/[ICARUS](https://www.icarus.mpg.de/en)) usage (GSM + GPS seem simpler so far, but satellites might be better on open sea)
- How to attach the transmitter to algae without causing tearing by e.g. waves. What has been done so far? (elastic straps?)
- Do we plan for a recoverable or a non recoverable device. (E.g. do the devices need a reciever?) (Value of information vs caused pollution.)
- Or would a radio drone be better aproach, with only radio tags needed for algae? https://wildlifedrones.net/how-it-works/
- The project plan itself is quite realistic as <50€ has been reached by other projects in tracking [birds](https://bioone.org/journals/wildlife-biology/volume-2020/issue-2/wlb.00653/Low-cost-DIY-GPS-trackers-improve-upland-game-bird-monitoring/10.2981/wlb.00653.full) and [Turtles](https://osf.io/jdrme/) and [cattle](https://onlinelibrary.wiley.com/doi/pdf/10.1002/ece3.4094)

**see the results of this discussion on the [assignment 02, Computer-aided design page](https://karlpalo.gitlab.io/fablab2021/assignments/assignment-02/)**




- This would be something to aim for (https://mdbuoyproject.wixsite.com/default/buoy, https://www.makerbuoy.com/about-1)
-or https://projectwilson2020.wixsite.com/mysiteS
- Also mdbuyou has some pricing info on the satellites (Iridium Service per Buoy: $10/mo, Iridium Messages per Buoy: $25/mo)
