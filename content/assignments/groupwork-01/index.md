+++
title = "Group assignment 01"
description = "Computer-controlled cutting"
+++

### Shape of the assignments

This week we had a group assignment, where the task was to characterize our lasercutter's:
- focus
- power
- speed
- rate
- kerf
- joint clearance and types


To get started I looked trough previous fab-academy works to see how other people had tackled these problems and how the entire testing process would look like. I ended up following [The work of Rinke van Couwelaar](http://fabacademy.org/2020/labs/waag/students/rinke-vancouwelaar/assignments/week04/#the-group-assignment-joint-clearance) and especially [the group assignment page](http://fabacademy.org/2020/labs/waag/groupAssignments/week3.html) linked on his fab-academy site.

This gave us a pretty good idea on how the other people had tackled this problem with their machine before.

### Working with the Aalto machine
The Laser cutter at Aalto was an Epilog Legend 36EXT.
{{< image src="lasercutter.jpg" alt="Image of the lasercutting machine at Aalto Fablab">}}{{</image>}}
#### Focus
The machine was equipped with an auto-focus functionality. Thus normally the issue of focus is not really focused on in this machine.

However, there was a manual focusing tool, that could be attached to the laser cutting head, to see at what depth would the focus point be.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="headnofocustool.jpg" alt="laser cutting tool head">}}The cutting head of the laser cutter{{</image>}}
  </div>
    <div class="p-2">
{{< image src="focusingtool1.jpg" alt="focus tool mounted on head">}}The cutting head with the mounted focus tool{{</image>}}
  </div>
</div>

Thus as the depth of the ideal focus was known, we decided to go up and down in ~2mm intervals to see the effect of losing focus. For testing the focus we made a vector-file (<a href="Focusline.svg" download>Focusline.svg</a>) with Inkscape that can be also found at the end of this page. For setting the line width there was some confusion on the internet, with values of 0.010 mm and 0.025 mm being recommended. We decided to go with 0.010 mm for no particular reason. To see the line (and not just cut trough the material) In the end this design choice ended up having no effect, as the final adjustments had to be done in the corel draw software connected to the directly to the cutter (setting the line width to hairline).


<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="focustest_front.jpg" alt="front of the focus test plate">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="focustest_back.jpg" alt="back of the focus test plate">}}{{</image>}}
  </div>
</div>

As one can tell from these the drifting off focus was quite severe, also the focus seemed to be less sensitive in the negative than in the positive direction. In the positive direction the +4mm and +6mm depths the cuts did not go through the 3mm plywood. Also at the +6mm depth there was a small flame present during the cutting so adjusting focus in that direction should be discouraged. After the tests I set the machine back to auto focus.

#### Power, Speed, Rate and the resulting kerf(size inaccuracy)
To test these 3 properties, and the kerf they produce, We decided to make a test piece with 20mm, 15mm, 5mm, 4mm, 3mm, 2mm, and 1mm squares mounted on a solid rod, with 25mm to 5mm squares containing the numerical value of the dimension in mm. These squares (and the quality of the cuts) could then be reviewed. (note, the actual vector file has the line diameter of 0.01mm, the image shown below has the line artificially set to 0.1mm for your viewing pleasure)
{{< vectorimage src="kerftestprint_for_viewing.svg" alt="image of the planned test print piece">}}{{</vectorimage>}}

For my test we decided to use 3 mm thick plywood, as I was planning to use it later in my individual assignment. As the ideal parameters for material were already recorded in our fablab, I decided to run the test by trying to cut the pieces with the local "ideal" settings for Power, Speed and Rate. One of these parameters would be varied at a time, while the other two would be kept at the ideal values.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="testmatrix.jpg" alt="image of the planned test matrix">}}{{</image>}}
  </div>
    <div class="p-2">
{{<image src="kerftests.jpg" alt="image of the test print piece">}}{{</image>}}
  </div>
</div>

#### Kerf results
Here are the final results for the kerf measured for each square of the each test piece

{{<image src="kerftable.jpg" alt="kerf results">}}{{</image>}}

Ultimately we only cared about the kerfs of the optimal test point, and for that the kerf width was 0,067mm
I used this value to update my parametric drawings of my personal assignement, and using this value caused all the joints to fit right out of the box, so this worked out fine!

For Hannu Ikola (the other member of my group) there was however substantial pain involved in his personal assignment, as he used 2mm plywood instead of 3mm one. This meant that the kerf that we measured in our group assignment was not good for his joints, and unfortunately this meant that he had to fiddle quite a lot with his individual work drawings.



Used files:
Inkscape:
- <a href="Focusline.svg" download>Focusline.svg</a>
- <a href="kerftestprint.svg" download>kerftestprint.svg</a>
