+++
title = "Assignment 07"
description = "Computer controlled machining"
+++
# Heroshot
{{< image src="heroshot.jpg" alt="kicad image">}}{{</image>}}

# First steps

For this assignment I decided to make a light fixture that would be useful in my PhD-project.

To get started I contacted the supplier of the lamp and asked if they could supply me with a CAD-model of their lamp. To my happy suprise, they replied promptly and provided me with the following model.

{{< image src="lamp_valoya.jpg" alt="kicad image">}}{{</image>}}

Thus instead of measuring the dimensions of the lamp, I could just design the fixture directly in cad to match the lamp chassis! This made work fast indeed!

# Starting the design

I wanted to have a fixture that would hold the lamp stable on a flat surface, pointing at a 90 degree angle, with the center of the light cone being at the height of approx 10 cm.

To start with my design, I used freecad to create a drawing around the CAD-model of the lamp.

{{< image src="fixture_hole.jpg" alt="kicad image">}}{{</image>}}

Using this drawing as a hole, I then designed a primitive fixture to keep the lamp in place.

{{< image src="fixture.jpg" alt="kicad image">}}{{</image>}}

The holes for the supports in the fixture were created parametrically, to match the thickness of the material I would eventually use.

To match these holes (and to create the structure) I also designed the jointed bars to go with my design. In my design I used one longer thicker bar to fix the structure, and two shorter bars to improve the stability.

{{< image src="supports.jpg" alt="kicad image">}}{{</image>}}

 I deemed this to be enough, as the lamp chassis itself, made of aluminium, would act as a structural support and rigifying element in my design.

 {{< image src="chassis.jpg" alt="kicad image">}}{{</image>}}

# Moving to CNC-toolpaths

I did stuff in freecad, such as:
<ul>
<li>broke up the original design file to individual parts</li>
<li>placed the components using Freecad optional modules</li>
<li>Created toolpaths</li>

</ul>
Basically I followed [this tutorial](https://www.youtube.com/watch?v=kbu4pEzIPl4) made by Krisjanis who works at the Aalto fablab.

Perhaps the only major exception was the milling of internal holes, as for the software to function properly, all the faces of the models "hole" had to be selected individually before creating an operation.

Here the big challenge was perhaps the CNC-machine itself. I had to check the specifications of the machine several times, and for my first toolpath trial I had made the parts too close to the machine limits --> thus unmillable.

After a short redo I started again, but in my enthusiasm to remake the toolpaths I forgot to edit the feed speed (and it was set to some extremely slow default rate by the machine). Here Krisjanis intervened as the slow milling would probably have made the wood burn!

 {{< image src="cnctoolpath.jpg" alt="kicad image">}}{{</image>}}

Finally I got everything in place and milled my piece.

 {{< image src="milledboard.jpg" alt="kicad image">}}{{</image>}}

However, some manual work was needed, as for some reason some of the toolpaths were not replicated correctly during the operation of the CNC-machine.

 {{< image src="unmilled.jpg" alt="kicad image">}}{{</image>}}

, the only flawed parts was in the insides of the joints, which were not carved properly.
 {{< image src="finalcut.jpg" alt="kicad image">}}{{</image>}}

Thus, by removing the excess wood, the mistake was easily corrected, and the lampholder could be assembled. Resulting in the finished product.

{{< image src="heroshot.jpg" alt="kicad image">}}{{</image>}}

## Files and toolpaths

<a href="models/BX-BL90 ASSY.stp" download>LAMP CAD-model from manufacturer</a><br>
<a href="models/Lampfixture_parametric.FCStd" download>Parametric freecad model for lamp fixture</a><br>
<a href="models/lampfixturecncmill.FCStd" download>Parametric freecad model for CNC-toolpaths</a><br>
<a href="models/toolpath_6mm_lampfixture" download>final G-code for milling</a><br>
