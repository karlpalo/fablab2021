+++
title = "Groupwork 04 - Observing the operation of a microcontroller board"
description = "Observing the operation of a microcontroller board"
+++

# Using the test equipment in the lab to observe the operation of a microcontroller board
There is several pieces of equipment in our lab one can observe the functionality of the board
<ul>
<li> Digital multimeter - for basic voltage/current </li>
<li> Oscilloscope - for analog signals</li>
<li> Logic analyzer -for digital signals </li>
</ul>
