+++
title = "Assignment 03"
description = "Computer-controlled cutting"
+++
## Laser cutting assignment

# "My Hero Shot"
This hero shot was taken from the complete laser cut table after putting some protective laquer on it.
{{< image src="tableassembly4.jpg" alt="finished table">}}{{</image>}}

### Looking for inspiration
For my inspiration I looked at two images i found on the internet.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="lasercutinspiration1.JPG" alt="Lasercut inspiration image 1">}} From this design I wanted to take the general structure and add rotating table legs{{</image>}}  </div>
    <div class="p-2">
    {{< image src="lasercutinspiration2.jpeg" alt="Lasercut inspiration image 2">}}From this image I wanted to take the cutting/engraving pattern{{</image>}}
  </div>
</div>
Finally I wanted to decorate the table front surface with a set of Mucha paintings, with mortise and tenon-type joints framing the paintings
{{< image src="mucha.png" alt="Lasercut inspiration image 2">}}{{</image>}}

### Thinking about joints
To join the pieces I would need to design joints. As I imagined, there are several types of joints one can use for laser cut structures, and I will link here some of my finds:
- [Laser cut like a boss](https://macythingspace.files.wordpress.com/2018/09/laser-cut-like-a-boss.pdf) An extremely good and comprehensive guide on laser cutting and laser cutting design. This is the guide I used on my work in joint planning and design!
- [Compliant joints](https://lasercutlikeaboss.weebly.com/uploads/2/7/8/8/27883957/advancedjoinery_master_web.pdf) A good tutorial on the mechanics and practicalities in making flexible or rootational joints (Unfortunately this resource did not really work for me, as the rotational joints had too complex structure to effectively manufacture just out of plywood)
-[Just cnc milled joints](https://makezine.com/2012/04/13/cnc-panel-joinery-notebook/) These joints were very useful as they gave me ideas on how to get the angled legs I wished for in my design.

- [Using wood expansion to seal joints](https://www.core77.com/posts/69981/How-to-Make-a-Watertight-Wood-Joint-Without-Using-Glue-or-Sealants) I was thinking could you use this technique in combination with the heat press (e.g. overdrying the wood, and letting natural swelling to seal the joint.) (Unfortunately this did not work with plywood (see groupwork assignment 01 to see why))

When I found the Laser cut like a boss tutorial I was very happy, as this included very detailed explanations of lasercutting, and after reading through the guide, I felt comfortable going forward with my design.

### Using freecad for design
As at the time of the design, I had not yet measured the kerfs and joint tolerances of the material I intended to use, I made a parametric drawing, where I could later set the values for the design of joints according to each material. For all the joints in the table below, by adjusting the kerf-values in the spreadsheets, the final drawings update their kerf to match the kerf adjustment set into the spreadsheet-page of the FreeCAD-drawing.

<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="holeparametric.jpg" alt="parametric hole">}}This is a screenshot from the table surface. For each hole I made in my drawing I used the spreadsheet value of kerf (that I specified after the group assignment) and removed 2*the amount of kerf from the length (and thickness) of each hole as the actual hole would be made larger by the laser.{{</image>}}
  </div>
    <div class="p-2">
{{< image src="tenonparametric.jpg" alt="parametric tenon">}}This is a screenshot from the table front panel with tenons. For tenons the parametric kerf correction had to be opposite. E.g. the correction had to be the planned length + 2*the amount of kerf. For tenons no width correction was needed, as the 3mm dimension was retained from the original material.{{</image>}}
  </div>
    </div>

<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="slidejoint.jpg" alt="slidejoint">}}This is a screenshot from the slide joint in the table side panel. In the design of these slide joints the keft thickness was parametrised. All other values were manually fiddled with, and I struggled quite a bit with the correct placement and design order of everything. My in experience in CAD-design was especially apparent here, and I finally ended up with the holes being slightly too short for a good fit (I had to file the holes manually a bit deeper later on.){{</image>}}
  </div>
    <div class="p-2">
{{< image src="tableleghole.jpg" alt="table leg hole">}}This is a screenshot from the table front panel with the hole for the leg support bar. At this point I was quite proficient with the placement and design of the holes, so designing holes with parametric kerf was no problem for the table or for the leg.{{</image>}}
  </div>
</div>

{{< image src="slidekey.jpg" alt="image of the key joint">}}This is a screenshot from the key joint. Notice that I made the outer "straight" wall of the key tilt ever so slightly outwards to ensure a more snug fit. (See the green arrow in the image.){{</image>}}


  {{< image src="tablecad.jpg" alt="Lasercut inspiration image 2">}}Here you can see 3 types of joints. A mortar-tenon type joint between the table plate and the square frame pieces. A press fit-joint with chamfers between the square frame pieces themselves and a double sided wedge joint fixing the legs to the square frame.{{</image>}}

The design became simpler than in the initial concept images due to using FreeCAD eating such a big chunk of my time. I am an slow amateur at CAD-design, and this slows me down, and forces me to go with more simple concepts when viable.


Also I encountered my first annoying software bugs when trying to export the SVG-files. When using the "Techdraw" workbench of reecad to export the .svg-images, I was unable to have more than one page of technical drawings open at once without the software getting confused on which page to export the 2D-snapshot of the CAD-model. Thus I had delete the other Techdraw-pages, and to take the parts individually to the initially made Techdraw-page, and then delete them after wards and repeating the process for each part. (Looked a bit into this in freeCAD forums, apparently it is a rare bug related to some chipset setups. It would have had a possible fix, but implementing the fix seemed too laborious to justify it. )
{{< image src="freecadbug.jpg" alt="image of the bug">}}{{</image>}}


Finally, the table will be decorated with an refurbished version of the original mucha image. (The image has been resized to fit inside the mortar-tenon type joints. Also I removed some tape markings from the first image using Gimps clone brush.)
    {{< image src="muchabw.png" alt="Mucha image2">}}{{</image>}}

Next I got down into lasercutting. First I printed out a small part of the parts to see if the joints worked properly. After that I aligned rest of the parts on the Corel worksheet, and then copied them to have the right amount of parts for the final assembly

### Lasercutting and assembling the table
When I first opened the .svg inkscape files in the coreldraw software connected to the laser cutter, I had some artefacts appearing in the drawings. This was resolved by resaving the files as .svg, but in the portale .svg format instead of inkscapes own svg format (I did not know several svg formats existed)

After solving this small issue I got to cutting. Tyhe table plate took a long while to cut, as the rastering took almost an hour (but the end result was quite gorgeous)
{{< image src="tablepic1.jpg" alt="raster image of the table">}}{{</image>}}

After the first parts were cut, I tested the side panels, and they fitted perfectly! My kerfing had been a success!
{{< image src="tableassembly1.jpg" alt="mounting side panels">}}{{</image>}}

However, the front and back sides of the frame had a bit of a problem, the U-shaped cut of the press joint on the front and back panels was exactly 3mm too short, so to mount them properly, I had to file the press joint cervasse a bit deeper on this part{{< image src="tableassembly2.jpg" alt="filing the press joint deeper">}}{{</image>}}

Rest of the assembly went without problems, but a new problem arose after the assembly the table in the image below was flimsy as hell (the legs were all wonky). This was because I used too thin plywood, and did not plan for the structural integrity and stiffness of the plywood legs.
{{< image src="tableassembly3.jpg" alt="assembled (wonky) table">}}{{</image>}}

This problem was resloved to the hero shot by using some spare round wood, cutting it manually to the width between two table legs, and attaching them via screws to the table legs. At the same time the wound wood rods made for nice handles to the table! So a flaw in design beame actually a positive thing. In the hero shot (at the top of the page), you can see the finished table, with the round support beam installed in the legs.






## Vinyl cutting assignment
For the vinyl cutter I decided to make a simple sticker for my laptop. I might get back to vinyl cutting again in the electronics week if I decide to make flexible conductors/PCB:s for my final project.

Nothing special here, found a nice picture on the internet, Imported it to Inkscape, converted it into an vector image and made the cut according to the local fablab specifications.
  {{< vectorimage src="algae.svg" alt="seaweed">}}{{</vectorimage>}}

The vinyl cut without problems, once the blade was adjusted to the proper depth and force, and after the test cuts the first cut ended up being the successful final product.

Picking the vinyl was surprisingly easy, as even though the pattern was complex, most of the complexity was on the outer edge. In practice this meant I just had to be careful when pulling out the unneeded vinyl parts (to not take away thin parts of the actual print), but all in all the process was quite fast, as there were less internal small parts to be scraped off than one would at first think.

To transfer the vinyl I used transfer tape, that had relatively poor adhesion to vinyl (compared to the adhesion of the vinyl glue and the laptop surface). The transfer tape was pressed on top of the vinyl, and as the adhesion between the vinyl background and the vinyl was worse than the adhesion between the vinyl and transfer tape, the vinyl print stuck to the transfer tape. On the laptop the process was repeated, but this time the transfer tape acted as the vinyl background.

# The Hero Shot
  {{< image src="herovinyl.jpg" alt="finished product">}}{{<image>}}




### Linked files

  Inkscape: <a href="tablebody.svg" download>tablebody</a>
  <a href="frontpanel.svg" download>frontpanel</a>
    <a href="tableside.svg" download>sidepanel</a>
    <a href="legbar.svg" download>leg attachment bar</a>
    <a href="leg.svg" download>tableleg</a>
    <a href="key.svg" download>leg attachment bar wedge</a>
    <a href="algae.svg" download>Vinyl cut algae drawing</a>



  Freecad: <a href="Table.FCStd" download>Table.FCStd</a>
