+++
title = "Assignment 09"
description = "Input devices"
+++

# background

For this week I decided to to things easy. My input device would be the final GPS-breakout chip I would use for my final project, and I would be using my personal arduino nano to read the input (GPS-data) on my computer.

My original plan was to experiment more with the various GPS-antennas (and compare them as input devices), but after reading the [datasheet for the GPS itself](https://cdn-shop.adafruit.com/datasheets/GlobalTop-FGPMMOPA6H-Datasheet-V0A.pdf) and [tutorials about the usage of the whole chip](https://learn.adafruit.com/adafruit-ultimate-gps/downloads) I came to the conlusion that GPS-antenna technology is much more complicated than I imagined, and that the chip I had contained already an [SAW-filter](https://en.wikipedia.org/wiki/Surface_acoustic_wave) for signal amplification. However, As there was an opportunity to plug-in various antennas (that would use the same signal processing treatment than the inbuilt antenna), It seemed that this board would be an really good piece for development and testing.

Hovever, to test the other antennas in a reasonable way, I needed to order some extra connectors, that would arrive at an later date.

## My setup

To get started I used a breakout board, where I plugged in arduino NANO and the adafruit Ultimate GPS breakout-chip. Then I read trough the [documentation for arduino Nano](https://components101.com/microcontrollers/arduino-nano) to make sure I would make the right connections. (I was already familiar with the GPS-chip documentation and connections). Then I made 4 jumper-cables using simple connectors and some electric cable, and connected my arduino and the GPS-chip.

{{< image src="breadboard.jpg" alt="image">}}{{</image>}}


## Looking at the connections and the Input signals

To get started, I had almost everythin ready. However, I had to set up the Adafruit GPS-library for arduino [according to the instructions here](https://learn.adafruit.com/adafruit-ultimate-gps?view=all)


I was lucky in the sense that everything worked out of the box. The GPS-chip sent out signals as expected.

{{< image src="data_recieving_nolocation.jpg" alt="image">}}{{</image>}}

However, to understand the data I had to read a little about [how GPS-data is typically encoded (NMEA)](http://aprs.gids.nl/nmea/)

{{< image src="data_recieving_location.jpg" alt="image">}}{{</image>}}

 and after staying connected for 5-10 min, could fix in the location with amazing accuracy. (perhaps 10m off from the location of the chip) ( X marks the real spot :D)

 {{< image src="location.jpg" alt="image">}}{{</image>}}
