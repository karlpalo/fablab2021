+++
title = "Assignment 04"
description = "Electronics production"
+++
## Making my own microchip

This weeks assignement was less of a creative one, and as the [group assignment](http://ikola.gitlab.io/fab-academy/electronics-production-group.html) introduced the mods system already, this will be more of a "fiilistelyviikko" In reporting

I'll start with the hero shot of the hero of the week
{{< image src="hero.jpg" alt="the chip">}}{{</image>}}

This hero was a painful guy to make, as I broke several tips in my attempts to make him. This chip you see here is the third attempt at tracing and the first at cutting (The cutting toolpath cut trough nicely the top layers, but on when moving down to the 3rd layer that would have completed the cut, the blade jammed and broke)

As the cut was almost there, I used some pincers to snap off the excess pieces of the chip (while wearing safety goggles, pcb shards are not nice in eyes)
This snapping resulted in the abberant shape of the chip.

{{< image src="antihero.jpg" alt="the damn chip">}}{{</image>}}

I also scraped off the excess copper using a razor, but in the process damaged a few filaments I had to replace with wire ( The places where there is a single strand of the multi strand wire are marked in yellow) Also to avoid my aberrant chip shape causing any trouble I soldered in an usb adapter.

As far as the soldering goes, I am somewhat experienced in that. Despite my experience however Kris did the most difficult part for me (soldering the microchip), as our lab currently has a limited amount of those and the delivery times are awful. I was for example able to repair the 2 broken traces without any outside help! :D
