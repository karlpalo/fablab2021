+++
title = "Group assignment 3"
description = "3D Scanning and printing"
+++
### Design rules of FDM 3D printing

As there are various types of 3D-printers available, each with their own intricacies regarding design rules In this group work we decided to focus on FDM-printing. This is the groupwork for Hannu Ikola and Karl Mihhels.

### Understanding the machine

An FDM-3d Printer is quite an complex machine, with several options to adjust the printing result. I myself (Karl) use a software called [PrusaSlicer](https://www.prusa3d.com/prusaslicer/), that works together nicely with the PrusaMk3S printer I have at home.

When one opens up the expert mode, there is truly a multitude of options to adjust, from heat control, to extrusion rate adjustments to fan speed adjustments to retractions during extrusion. It is also possible to add G-code (the code snippets that make the machine operate) manually to have even more control over the printer.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="prusaslicer1.jpg" alt="prusaslicer intro">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="prusaslicer2.jpg" alt="prusaslicer intro">}}{{</image>}}
  </div>
</div>

Luckily not everything needs to be understood to print successfully. However, tuning and playing around with these parameters may help you in producing higher quality prints.

### Testing the machine

To test the capabilities of and FDM-3d printer, we decided to use an [existing test](https://github.com/kickstarter/kickstarter-autodesk-3d/tree/master/FDM-protocol). The test in question has been developed in collaboration with Autocad, and is aimed especially at benchmarking different printers.

The test tests for following properties:
<ul>
    <li> Dimensional accuracy </li>
    <li>  Negative feature resolution</li>
    <li>  Positive feature resolution/fine flow control</li>
    <li>  Basic overhang capabilities</li>
    <li>  Basic bridging capabilities</li>
    <li>  XY ringing</li>
    <li>  Z-axis alignment</li>
</ul>
    {{< image src="testmodel.jpg" alt="image of the .stl model">}}This is how the test model should look like in ideal case.{{</image>}}

#### Dimensional accuracy test
The dimensional accuracy test was conducted by measuring the diameters of a stack of cylinders both along the X and y axis of the print.
{{< image src="dimaccuracy.jpg" alt="dimaccuracy test">}}{{</image>}}
The Prusa MK3S performed fairly well, scoring 4/5 according to the [existing test](https://github.com/kickstarter/kickstarter-autodesk-3d/tree/master/FDM-protocol).

This type of test is especially important after setting up your 3D printer, as it reveals if there are problems with the dimensional accuracy. Furthermore, you get an idea if the difference is centered on a single axis (suggesting a problem with the axis).

#### Negative feature resolution
The negative feature resolution test was conducted by seeing if pins could be pushed out from pinhole, with the given print tolerances. (0.2-0.5mm)
In our test, immediately after lifting the print 4 out of the 5 pins fell out by themselves, and the last pin (0.2mm tolerance) fell out after pressing it lightly with fingers, giving the Prusa MK3S the full 5/5 score.
{{< image src="negres.jpg" alt="negative resolution test">}}{{</image>}}

This type of test gives you an idea of the tolerances you need to have if you want to have 3D parts with movement in them (such as print-in-place gears and hinges), this value is critical. In my case, for gears the tolerance should probably be 0.3mm, as 0.2mm needs some force to separate, and the force used to separate might damage the structure if it is very fine.

#### Positive feature resolution/fine flow control
This test had 2 parts
<ul>
<li>Positive feature control - tested by seeing if all the spikes printed over 30mm higher</li>
<li>Fine flow control - tested by seeing if there is any stringing between the peaks</li>
</ul>

{{< image src="peaks.jpg" alt="positive features and fine flow control test">}}{{</image>}}

Here the [existing test](https://github.com/kickstarter/kickstarter-autodesk-3d/tree/master/FDM-protocol) instructed that 2.5 points were awarded if all the peaks were printed high enough, and a further 2.5 points were added if there was no stringing. As it can be seen that PrusaMk3S printed all the peaks properly, but there was stringing, total score was 2.5 points.

This type of test gives you an idea how your printer performs when printing complex objects. Can you expect stringing, requiring post-print cleanup, or will you instead experience incomplete reproductions of details. I think you will most likely experience one or the other, and I think it will have much to do with the flow properties of your filament. Thus repeating this part of the test at different temperatures might be worthwhile. Furthermore, there are some further tricks one can play to avoid stringing, such as using retractions (e.g. the printer head lifts rapidly up before starting the move to the next location, breaking the stringing filament)

#### Basic overhang capabilities
This test was in principle very simple. The test print included a ramp, printed with 45,30,20 and 15 degree overhangs.
The test gave points as follows. 1 point if there were dropped loops or clear print errors, and then 3,4 and 5 points corresponding to the print surfaces under the overhangs at various degrees.

{{< image src="degs.jpg" alt="overhang test">}}{{</image>}}

Here the PrusaMk3S performed relatively poorly, giving only 3 points, as the both 15 degree and 20 degree overhang had clear differences in their print surface to the 45 degree one.

This type of test gives you a good idea how your printer handles overhangs, and what is the expected amount of loss in print quality if your overhangs become quite extreme. As a general design rule, in 3d printing, overhangs should be made to have steep enough support angle in the modelling phase, and the model should be printed in a direction that minimizes steep overhangs. (or then one must use supports)

#### Basic bridging capabilities
Another simple but vital test. This test printed bridges with the length of 1,2,3,4 and 5 cm. The test was considered to be passed if the bottom of the higher bridge left an air gap in the print. Here PrusaMk3S performed really well, giving the full 5 points.(even though the point 5 was a really close pass!)

{{< image src="bridge.jpg" alt="bridging test">}}{{</image>}}

The bridging tests the printers capability to bridge gaps. This property becomes very important especially when printing complicated geometries. There some tricks however one can try to tune their 3d printer to "bridge in air", usually involving slowing the extrusion and movement speed of the print head, and increasing the cooling of the filament to allow for less sag during the construction of the first layer of the bridge.

#### X-Y Ringing
Here the PrusaMk3S should have performed well, not showing any ringing at all, but the machine we used showed ringing effects in the Y-axis!
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="xaxis.jpg" alt="xaxis ringing test">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="yaxis.jpg" alt="yaxis ringing test">}}{{</image>}}
  </div>
</div>
The resonance usually tells of some kind of looseness or bad adjustment in the system, so it might be a good idea to check the tension of the Y-belt and the attaching nuts and bolts in the Y-axis.

#### z-axis alignment
This test gave the full 5 points to PrusaMk3s. In this test one tried to observe any periodic effects in the print quality along the z-axis. No such effects were observed.

{{< image src="zaxis.jpg" alt="z axis alignment test">}}{{</image>}}

Here, if any effects were visible, they would be typically be caused by the stepper motor construction, (e.g. all the steps are not equal, but instead a cycle of e.g. 30 steps is always the same length)


#### Final words

It was fairly interesting to run these tests, and to figure out what they were actually testing (and to hypothesise ways to "hack" the tests to get better results with the same machine.)
