+++
title = "Assignment 05"
description = "3D Scanning and printing"
+++

## The scope of this week

This week I actually did something useful. My dad runs a [cleaning equipment company](camee.fi) that sells machines like this:

{{< image src="lhkone.jpg" alt="the problem">}}{{</image>}}

However, this machine here is a brand new one available for purchase right now. What happens in real life is that there are some machines from the 80s brought in sometimes to the shop, with a broken plastic handle (see the marked part in the previous image). The handle in question, when pressed releases the lock between the handle bar and the machine chassis, allowing you to slide the machine under low objects when needed.

As spare parts from the 80s are no longer available, a typical fix is to bolt the upper part of the machine to the lower part at a fixed angle (e.g. 45 deg). Thus losing some of the original ergonomics and functionality of the machine.

Thus my project was that I got 3 plastic parts from an old machine and tried to reproduce those. Especially the handlebar was expected to be difficult, as a competitor of my dads had tried to cnc-mill a metal copy of it and the attempt had failed. (Also the milled part was too expensive for this type od machine.)

### The parts to be reproduced

Unfortunately I sucked in my documentation and forgot to take a picture of the original parts by themselves, so you will see the results of my work straight away! The original parts are in gray and white plastic while the 3d printed ones are in black.

{{< image src="the parts.jpg" alt="the final result">}}{{</image>}}


### Getting There

I started my process with great enthusiasm: I would 3D scan these objects and then 3D print them right away.

This did not happen.

The results were mediocre at best, and to even those I needed to use reference objects along the scanned objects (See the magnificent foam COW!) and also use colorful tape and post it notes to create detail for the 3D scanner to interpret (especially the dark grey object was unscannable without adding some lines with blue masking tape on it). Unfortunately I forgot to document these markings. GO ME! But at least you know that there was such a thing in existence.

Here you can see a result of the scans (a screenshot taken from 3D software called [blender](https://www.blender.org/))

{{< image src="scans.jpg" alt="the scanned objects">}}{{</image>}}

As this quality was subpar for parts that needed to actually function (and needed to have exact holes etc.) I used blender to model the objects
However, I used the 3d scans a bit as I modelled the rough version of the object "on the scan". However,  to get the exact measures I used a caliper to measure various lenghts of the objects I used in the final 3D model.

{{< image src="inside.jpg" alt="using the scanned object as a referece">}}{{</image>}}

The modelling process itself was mainly done by boolean processes or vertex manipulation.

In boolean I modelled 2 pieces, and used 1 of them to cut away pieces from the other.

<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="boolean1.jpg" alt="boolean before cut">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="boolean2.jpg" alt="boolean after cut">}}{{</image>}}
  </div>
</div>

In vertex edit mode I moved individual vertices around to match the rough shape of the scan.
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="vertex1.jpg" alt="vertex before move">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="vertex2.jpg" alt="vertex after move">}}{{</image>}}
  </div>
</div>

Thus I ended up with 3 3D-objects that were quite nice!

{{< image src="nice.jpg" alt="demoshot">}}{{</image>}}

As you can see in the reveal shot at the start, these objects mached the original ones quite well

### Why not use CNC?

{{< image src="cnc trouble.jpg" alt="demoshot">}}{{</image>}}

Here is a snapshot of the handle showing the part that would be difficult to manufacture using a conventional CNC-tools.
As the part has several holes and features (facing in different directions), It would need a very well calibrated 5 axis cnc-machine to have any hope of reproducing this feature of the part. With 3D printer you might be able to print it without supports, if the overhangs do not become too great (however I used supports to ensure print quality.)


### A mishap

However, there was one more fuckup in my process. And this happened in the 3D slicing software.

As the handle was first printed facing up (so it needed minimal supports) the z-layers were along the direction of maximum force in the handle.
The consequence of this was that the handle delaminated during initial testing.

The second version of the handle was printed so that the mechanically weakest axis (z-axis) would be peripendicular to the force used to operate the handle. This resulted in much better operation and strength properties of the handle.

{{< image src="axismaxis.jpg" alt="broken handle">}}{{</image>}}

### Linked files
Blender:
<a href="parts.blend" download>parts.blend</a>
STL:s for printing:
<a href="part1.stl" download>part1.stl</a>
<a href="part2.stl" download>part1.stl</a>
<a href="handle.stl" download>handle.stl</a>
