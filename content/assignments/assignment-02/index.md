+++
title = "Assignment 02"
description = "Computer aided design"
+++
## **This site is for the individual weekly assignments.**
**If you are looking for my Final Project page, click [Here](../../final-project)**

##Design of the device.
After discussing the topic with biologists, several design parameters became clear!

Size and shape:
- The device needs to be attached to the leaf of algae, and should not disturb the movement of the algal ball too much. (The algal ball is from pea-size to a cauliflower-size) This means that acceptable dimensions could be c.a. 4cm*1-0.5cm (as small as possible).
- The device will be tied to algae using dental floss. --> the chassis would need to contain tying points.

Functionality:
- It needs to float with the algae ball (depth is typically 1-2m underwater) for a pre-fixed amount of time, then give a PING of the location it ended up in
- Salinity: 5.2-6.6 PCU for buoyancy tests
- Temperature 10-20C for buoyancy tests

Technical solutions
- 2 part system, with a heavier "sled" and a floating "transmitter"
- Note: Air pockets are not good --> The floater needs to be filled with a liquid?
- The transmitter remains non-operational for a fixed amount of time, after which it will rise to the surface by detaching from the sled.
- Detachment by for example a magnetic/electric coil? Linear actuator? Solenoid switch?

### First drafts
I decided to draw the first drafts of the device by hand, and then take a picture of my hand drafts, clean them up a bit in gimp, and make then into 2D concept images using inkscape.

After that I will be using FreeCAD to make a prototype piece. At this point I will not be thinking about the internal assembly, except for the most basic size limitations (Size of the USB and GSM-chips, a motherboard and a battery)

Approximate sizes:
- GPS: [~ 25mm x 25mm * ~5mm](https://www.digikey.fi/product-detail/fi/adafruit-industries-llc/4415/1528-4415-ND/10709724)
- LoRaWAN [51mm x 23mm x 8mm](https://www.digikey.fi/product-detail/fi/adafruit-industries-llc/3179/1528-1706-ND/6098604)
- Motherboard[35.0mm x 17.0mm x 4.2mm](https://www.digikey.fi/product-detail/fi/adafruit-industries-llc/3675/1528-2500-ND/8031669)
- Battery (AA)  50,5mm * 14,5mm diam. (cylinder) 2700mAh(alkaline), 2000mAh(NiMH)
- Battery (C)  50,5mm * 26,2mm diam. (cylinder) 8000mAh(alkaline), 6000mAh(NiMH)
- Solar panel?
- DC-engine: [20mm*24,00mm diam. (hull cylinder) + 10mm * 3mm diam.(axis cylinder)](https://www.digikey.fi/product-detail/fi/seeed-technology-co-ltd/108990003/1597-1203-ND/5487797)

### Gimping out

At first, I used Gimp to open up the image taken from my cell phone, to edit down to a size suitable for publishing on my website. I also will be using gimp to downscale all the other images published in this report. The achieved savings in filesize were quite significant as the filesize of the original image was 2.41 MB, and I got it down to 24.1 KB. For a single picture this is not a huge thing, but considering if I do this for every picture on the site, the eventual size savings get remarkable.

 To achieve this, in gimp, I simply opened up the original image from the dropdown menu option *file --> Open* and navigated to my file location to open the file. Then I cropped the image to the shape I wanted, so it would include the elements I demonstrated using the [crop-tool](https://docs.gimp.org/2.10/en/gimp-tutorial-quickie-crop.html). Finally I set the image width arbitrarily to 800px ([600-800px size is recommended for web use by the gimp site])(https://docs.gimp.org/2.10/en/gimp-tutorial-quickie-scale.html)  using the [scale-tool](https://docs.gimp.org/2.10/en/gimp-tutorial-quickie-scale.html) for all the images used on this website to make it reasonable quality for both large and small monitor-sizes.

Finally I used the export-as function and exported the images at a 45-quality setting using a .jpg compression. (See pictures below) These screenshots were provided using the same method mentioned above. However I should mention that using gimp to edit screenshots of gimp got very quickly very confusing. I sometimes clicked the interface buttons visible in the image, and all in all, producing even these 2 simple screenshots taxed my brain so that I decided to not take more pictures of the usage of the software.

 After the image was opened opened in gimp
<div class="d-flex flex-row">
  <div class="p-2">
{{< image src="gimpusage1.jpg" alt="gimp usage 1">}}{{</image>}}
  </div>
    <div class="p-2">
{{< image src="gimpusage2.jpg" alt="gimp usage 2">}}{{</image>}}
  </div>
</div>

Finally, below you can see my almost professional initial sketch of the device. I think I should look for career alternatives outside drawing art. However, drawing the image really made me think, how to attach the 2 parts of the device to each other and what kind of mechanism would operate the de-attachments. Using servos feels a bit excessive (and they take a lot of space!). With magnets there is risk of unintended deattachment (if magnet is too weak) and reattachment (if magnet is too strong). Also quick googling produced no nice results. I think I will raise an issue in the aalto fablab course forum.
<p>

{{< image src="initialdesign.jpg" alt="initial pen and paper design">}}{{</image>}}

### Inkscaping in

Then I tried doing the lazy thing. [(E.g. importing my image to inkscape and using convert bitmap to vector to see how it worked.)](https://inkscape.org/doc/tutorials/tracing/tutorial-tracing.html) I also changed the colour of the elements to white, and saved the image as .SVG to demonstrate the transparency that vector graphics allow. If you later see vector images with a white background, know that that white background comes from the .html code, not the image itself!
{{< image src="drawing1.svg"   alt="initial vector image">}}{{</image>}}

But here my lack of drawing skills is apparent. If I had made drawings with technical precision, now I could have taken advantage of that. However, due to the crudeness of my drawings, it makes more sense to start from zero in inkscape than to try to tweak the existing image.

I started by turning of the fill, and using the circle tools [switch to segment option](https://wiki.inkscape.org/wiki/index.php/Circle/Arc_Toolbar). Then I converted the object to path, and deleted and disconnected nodes to get an open half-circle. After that I used the [pen tool](https://wiki.inkscape.org/wiki/index.php?title=Inkscape_glossary#Pen_tool) to draw some straight lines at approximately 45 degree angle. Once I had drawn these elements, I could use the built in snap tools to copy, paste and move the elements to assemble a faux-orthographic image of my components.

{{< image src="inkscape1.jpg" alt="showing assembly in inkscape">}}{{</image>}}

Also, It became a convenient practice to store a set of "lines" outside the image canvas area, as I could easily copy and paste the pieces from there to the actual image.

The only issue I had was that some lines were visible even though they were supposed to be covered by other objects. However, I solved this by creating another layer on top of the main drawing layer. Copying the outlines of the solid front surfaces, combining these outlines to a single object, and filling the object with white colour. You can obseve the resulting ortographic scetches below.

{{< vectorimage src="drawing2.svg" alt="showing assembly in inkscape">}}{{</vectorimage>}}


### Starting the real stuff in FreeCAD

For start I will try modelling the "mandatory components" e.g. the parts mentioned in the "first drafts"-section in freecad.
I modelled these parts using [a basic tutorial to help me](https://wiki.freecadweb.org/Creating_a_simple_part_with_PartDesign) and then moved them to have a sort of an idea how big the assembly would be.
{{< image src="freecad1.jpg" alt="showing assembly in freecad">}}{{</image>}}

Then I continued the assignment by starting to create the basepart of the floater by [specifying dimensions via spreadsheet](https://wiki.freecadweb.org/Manual:Using_spreadsheets) In the image below you can see how the spreadsheet and the length of the floater cylinder are connected. The vital part here was creating the alias for the cells in the B-column of the spreadsheet (the alias I used can be seen in the A-column). By changing the value in the B-column, the actual dimensions of the drawing change. Also, it might make sense to organize the spreadsheets in a way that makes the editing easy.

{{< image src="freecad2.jpg" alt="showing spreadsheet functionality in freecad">}}{{</image>}}

Unfortunately this week I did not have time to freecad the model further, but I will get back to this. The freecad-file (as it is here) can be found at the end of this Page

### Linked files
GIMP: <a href="freecad2.xcf" download>freecad2.xcf</a>
- Note, only 1 file included as an example as GIMP-files are quite large. This file just demonstrates the use of layers that were used in constructing freecad2.jpg
- The .jpg files can be accessed directly from the page above by right-clicking on the images.

Inkscape: <a href="drawing1.svg" download>drawing1.svg</a>
<a href="drawing2.svg" download>drawing2.svg</a>
- Note, Some layers had been used in organizing the images, but the use and naming of layers is not really smart. This is something to do better next time.

Freecad: <a href="buoy_vers_alpha.FCStd" download>buoy_vers_alpha.FCStd</a>
