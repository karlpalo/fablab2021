+++
title = "Assignment 00"
description = "Principles and practices, The initial project proposal"
+++

## **This site is for the individual weekly assignments.**
**If you are looking for my Final Project page, click [Here](../../final-project)**

#Initial words

I started my project by getting familiar with the framework of fablab, and reading the [fab charter](http://fab.cba.mit.edu/about/charter/)

I am slightly familiar with the Aalto Fablab due to my earlier work in teaching kids in the [Aalto Junior-program](https://www.aalto.fi/fi/aalto-yliopisto-junior). There I was responsible for running the chemistry activities from 2018 to 2020. I then contacted fablab to get some photoactive resin for student works, and Kris gave me some resins from the formlabs printer that had aged (e.g. they were not reliable for printing). However, for students investigating polymerisation and UV-curing they were more than enough!


# The final project page
I decided to keep everything related to my final project on its own project page, so future fabbers who read through my documentation will not have to dig the data from multiple sites. (e.g. the mandatory assignment work will be found with assignments, and the project work will be on its own page)

[The sketches and the plan for my final project can be found here](../../final-project)**
