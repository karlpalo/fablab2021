+++
title = "Assignment 01"
description = "Principles and practices, presentations, introductions"
+++

## **This site is for the individual weekly assignments.**
**If you are looking for my Final Project page, click [Here](../../final-project)**


Setting up my project website and git repository
====================================================
The purpose of this weeks work was to set up the project infrastructure, and to also become familiar with the tools one can use to do that.

In the past, I have tried setting up websites via wordpress, and done some haphazard project management (in the windows file browser), but lets see if I can actually get to a level that would be suitable for some real work.


Setting up Git & Gitlab
------------------------------------

The first step was setting up a repository/version management program called [Git](https://git-scm.com/) and the connection of that program to an online repository hosted by [Gitlab](https://gitlab.com/)

### setting up Git

In the case of [Git](https://git-scm.com/) this required me to just download the windows-version of the program and install it. The program GIT included a command prompt-setup called "Git CMD" which I used, until I went over to a program called "CMDR" (more on that later)

### setting up Gitlab

For Gitlab I needed to create an account and a new project repository. I picked for my account name an nickname I have been using in online gaming: `Karlpalo` and for my project name: `Fablab2021`. Thus my project address became [https://gitlab.com/karlpalo/fablab2021](https://gitlab.com/karlpalo/fablab2021)

To test the gitlab functionality, I created an ultra-simple index.html file you can see below.
```
<!doctype html>
<html>
  <head>
    <title>Boo</title>
  </head>
  <body>
Hello Mr Potato!
  </body>
</html>
```
After uploading this file to the repository via the web interface, I also used the web interface to set up a file called `.gitlab-ci.yml`. This file was set up via the instructions in the [Fablab tutorial video](https://www.youtube.com/watch?v=sfFYqPGfUZU&list=PLruSKIDLbfPxrDSv6z_lCqthjzaSFddMT&index=3).

As an end result, I had a real existing project website at: [https://karlpalo.gitlab.io/fablab2021/](https://karlpalo.gitlab.io/fablab2021/)

### Connecting Git to Gitlab
To actually use the Git and Gitlab as inteded I needed to connect the two with each other.
In practice this is done by giving your computers SSH-key (which acts as an identifier) to the gitlab website.

To achieve this I had to set up an SSH-key for my Windows computer, and I used [these instructions](https://phoenixnap.com/kb/generate-ssh-key-windows-10) to do so. One probably should google though for how to do this for your operating system, as the details may vary over time slightly.

After generating the SSH-key, I opened the file and copy-pasted the contents to the Profile --> Settings --> SSH-keys page.

### Setting up the local repository

Then I opened my `git CMD` and got busy. As I decided to be lazy, I decided that my working directory would be the default directory of the CMD (e.g. C:/Users/myusername/), so using the clone command of Git
```
git clone https://gitlab.com/karlpalo/fablab2021
```
I copied the contents of the Gitlab directory to my local computer, and set up the connection. (All of this worked because I had told the Gitlab website via the SSH-key that connecting my computer like this is OK)

### Getting into it like a Git
Next I was basically just fooling around with the git basic commands, while going through the [Fab academy 2021 youtube channel for Aalto](https://www.youtube.com/playlist?list=PLruSKIDLbfPxrDSv6z_lCqthjzaSFddMT) This ended up with a basic website at the repository, and a local copy at my computer. Made some more .html -sites to test linking, and then destroyed the whole project so far, to start anew with HUGO  

### Getting my CMDR in place
To make it easier to follow the lectures, and to use the materials available online, this was the point where I installed [CMDR](https://cmder.net/). CMDR is a CMD-interface, that allows the emulation of an unix-type console in windows. Installation of CMDR was fairly straightforward.

### Finding out that I need still one more thing
And this thing was a programming language called GO. You can find the installation kit at their [website](https://golang.org/). Just download, install and you'll be golden.

### Installing Hugo and setting the path
Installing hugo was super easy, I just dropped the Hugo.exe in a folder I created. However, to connect CMDR to hugo (and other CMD prompts as well) I needed to add a %Path% variable to the windows OS, to tell that this is the place where you can find the .exe file to run via CMDR. This I did via an [online tutorial](https://gohugo.io/getting-started/installing/#windows) Only difference was I put the hugo.exe into a folder called C:/hugo. (by running the following commands at C:\)
```
git clone https://github.com/gohugoio/hugo.git
cd hugo
go install --tags extended
```

### Actually installing Hugo and testing the hugo server
After this I used CMDR to navigate to my local FABLAB2021 project folder and used  
```
hugo new site
```
To create my site. Then I followed the [Fablab-tutorial](https://www.youtube.com/watch?v=7wCidM35tp8&list=PLruSKIDLbfPxrDSv6z_lCqthjzaSFddMT&index=4) to set up my site, and the command
```
hugo server
```
to start a local copy of the website.

After plenty of fiddling and diddling, I ended up with a rough copy of the website you see here.

### Installing Bootstrap and filling in the menu bar
The last thing I did that was that instead of the ugly basic blue hyperlinks, I used a [fablab tutorial on bootstrap](https://www.youtube.com/watch?v=Ea1xNusD5f8&list=PLruSKIDLbfPxrDSv6z_lCqthjzaSFddMT&index=6) To set up some fancy .css and . js functionality. To do this I used Bootstrap, a ready made js/csss package that I just needed to plug into hugo to take advantage of. you can see now on the website. (The navigation bar is fancy, and the picture of me on the about page has been set to an column covering 1/3rd of the screen via Bootstrap.)

### Starting to play around with the graphics

{{< imagetoside src="website1.png" alt="my website after fablab tutorials">}}
This is how my website looked at the start
{{< /imagetoside>}}

After creating a karlstyle.css-file to override the css-instructions made by bootstrap.min.css I was good to go
The important thing however was to open the karlstyle.css after bootstrap.min.csss
```
/*Excrept from index.html*/
<link rel="stylesheet"  href="{{.Site.BaseURL}}/assets/css/bootstrap.min.css">
<link rel="stylesheet"  href="{{.Site.BaseURL}}/assets/css/karlsite.css">

```
However, this did not work for the navigation bar, for that I had to add the styling instructions into the shortcode file navigation.html via
```
<style></style>
```
tags at the beginning of the navigation.html page. At this point my site is black, green and shines a little. Good enough for week one I say!
