EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:MOSFET_N-CH_30V_1.7A Q1
U 1 1 60B7438E
P 4900 3400
F 0 "Q1" H 5008 3442 45  0000 L CNN
F 1 "MOSFET_N-CH_30V_1.7A" H 5008 3358 45  0000 L CNN
F 2 "fab:SOT-23" H 4930 3550 20  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS355AN-D.PDF" H 4900 3400 50  0001 C CNN
	1    4900 3400
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_GND #PWR0103
U 1 1 60B7577B
P 3500 1650
F 0 "#PWR0103" H 3500 1400 50  0001 C CNN
F 1 "Power_GND" H 3505 1477 50  0000 C CNN
F 2 "" H 3500 1650 50  0001 C CNN
F 3 "" H 3500 1650 50  0001 C CNN
	1    3500 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3250 4700 3250
Wire Wire Line
	4700 3250 4700 3500
Wire Wire Line
	3500 1650 3500 1250
Wire Wire Line
	3500 1250 3800 1250
Text GLabel 3800 1250 0    50   Input ~ 0
GND
Wire Wire Line
	2750 4150 2750 4550
$Comp
L fab:R R2
U 1 1 60B85265
P 4100 3650
F 0 "R2" H 4170 3696 50  0000 L CNN
F 1 "R" H 4170 3605 50  0000 L CNN
F 2 "fab:R_1206" V 4030 3650 50  0001 C CNN
F 3 "~" H 4100 3650 50  0001 C CNN
	1    4100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3350 4100 3350
Wire Wire Line
	4100 3350 4100 3500
Wire Wire Line
	4100 3800 4100 4100
Wire Wire Line
	3800 4100 3800 4550
Wire Wire Line
	5350 5250 5350 5300
Wire Wire Line
	4700 5250 4900 5250
Wire Wire Line
	4400 5550 4400 5800
Wire Wire Line
	4400 6000 4150 6000
Text GLabel 4150 6000 0    50   Input ~ 0
GND
Text GLabel 1750 2750 0    50   Input ~ 0
3.3V
Text GLabel 5350 5300 0    50   Input ~ 0
3.3V
$Comp
L fab:Regulator_Linear_ZLDO1117-3.3V-1A U2
U 1 1 60B91BD1
P 4400 5250
F 0 "U2" H 4400 5492 50  0000 C CNN
F 1 "Regulator_Linear_ZLDO1117-3.3V-1A" H 4400 5401 50  0000 C CNN
F 2 "fab:SOT-223-3_TabPin2" H 4400 5450 50  0001 C CNN
F 3 "http://www.diodes.com/datasheets/AP1117.pdf" H 4500 5000 50  0001 C CNN
	1    4400 5250
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD J1
U 1 1 60B966E0
P 3600 2300
F 0 "J1" H 3612 2525 50  0000 C CNN
F 1 "Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD" H 3612 2434 50  0000 C CNN
F 2 "fab:PinHeader_UPDI_01x02_P2.54mm_Horizontal_SMD" H 3600 2300 50  0001 C CNN
F 3 "~" H 3600 2300 50  0001 C CNN
	1    3600 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 4550 3300 4550
Wire Wire Line
	3300 4550 3800 4550
Connection ~ 3300 4550
Text GLabel 3350 4250 0    50   Input ~ 0
GND
Wire Wire Line
	3300 4550 3300 4250
Wire Wire Line
	3300 4250 3350 4250
Text GLabel 3800 2600 0    50   Input ~ 0
GND
Wire Wire Line
	3800 2300 4300 2300
Wire Wire Line
	4300 2300 4300 3150
Wire Wire Line
	3350 3150 4300 3150
Wire Wire Line
	3800 2400 3800 2600
$Comp
L fab:Power_PWR_FLAG #FLG0101
U 1 1 60B9C113
P 2450 6200
F 0 "#FLG0101" H 2450 6275 50  0001 C CNN
F 1 "Power_PWR_FLAG" H 2450 6373 50  0000 C CNN
F 2 "" H 2450 6200 50  0001 C CNN
F 3 "~" H 2450 6200 50  0001 C CNN
	1    2450 6200
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_PWR_FLAG #FLG0103
U 1 1 60B9E025
P 2450 6300
F 0 "#FLG0103" H 2450 6375 50  0001 C CNN
F 1 "Power_PWR_FLAG" H 2450 6473 50  0000 C CNN
F 2 "" H 2450 6300 50  0001 C CNN
F 3 "~" H 2450 6300 50  0001 C CNN
	1    2450 6300
	-1   0    0    1   
$EndComp
$Comp
L fab:Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD inputPwr1
U 1 1 60B9EACD
P 2100 6200
F 0 "inputPwr1" H 2112 6425 50  0000 C CNN
F 1 "Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD" H 2112 6334 50  0000 C CNN
F 2 "fab:PinHeader_UPDI_01x02_P2.54mm_Horizontal_SMD" H 2100 6200 50  0001 C CNN
F 3 "~" H 2100 6200 50  0001 C CNN
	1    2100 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 6200 2450 6200
Wire Wire Line
	2300 6300 2450 6300
Text GLabel 2750 6300 2    50   Input ~ 0
GND
Text GLabel 2750 6200 2    50   Input ~ 0
PWR
Text GLabel 2950 5250 0    50   Input ~ 0
PWR
Connection ~ 2450 6200
Wire Wire Line
	2450 6200 2750 6200
Connection ~ 2450 6300
Wire Wire Line
	2450 6300 2750 6300
Wire Wire Line
	2950 5250 3400 5250
Wire Wire Line
	3450 5250 3450 4800
Connection ~ 3450 5250
Wire Wire Line
	3450 5250 4100 5250
NoConn ~ 3350 3750
Wire Wire Line
	1750 2750 2750 2750
NoConn ~ 3350 3650
$Comp
L fab:LED D1
U 1 1 60BAD1A4
P 3950 4100
F 0 "D1" H 3943 4316 50  0000 C CNN
F 1 "LED" H 3943 4225 50  0000 C CNN
F 2 "fab:LED_1206" H 3950 4100 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 3950 4100 50  0001 C CNN
	1    3950 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3200 4900 3200
Wire Wire Line
	3450 4800 6250 4800
$Comp
L fab:R R1
U 1 1 60C09FF4
P 6250 4000
F 0 "R1" H 6320 4046 50  0000 L CNN
F 1 "R" H 6320 3955 50  0000 L CNN
F 2 "fab:R_1206" V 6180 4000 50  0001 C CNN
F 3 "~" H 6250 4000 50  0001 C CNN
	1    6250 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 3600 4950 3600
Text GLabel 4950 4350 2    50   Input ~ 0
GND
$Comp
L fab:C C1
U 1 1 60C0E09E
P 3450 5650
F 0 "C1" H 3565 5696 50  0000 L CNN
F 1 "C" H 3565 5605 50  0000 L CNN
F 2 "fab:C_1206" H 3488 5500 50  0001 C CNN
F 3 "" H 3450 5650 50  0001 C CNN
	1    3450 5650
	1    0    0    -1  
$EndComp
$Comp
L fab:C C2
U 1 1 60C0F15D
P 4900 5700
F 0 "C2" H 5015 5746 50  0000 L CNN
F 1 "C" H 5015 5655 50  0000 L CNN
F 2 "fab:C_1206" H 4938 5550 50  0001 C CNN
F 3 "" H 4900 5700 50  0001 C CNN
	1    4900 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5250 4900 5550
Connection ~ 4900 5250
Wire Wire Line
	4900 5250 5350 5250
Wire Wire Line
	3400 5250 3400 5500
Wire Wire Line
	3400 5500 3450 5500
Connection ~ 3400 5250
Wire Wire Line
	3400 5250 3450 5250
Wire Wire Line
	3450 5800 4400 5800
Connection ~ 4400 5800
Wire Wire Line
	4400 5800 4400 5850
Wire Wire Line
	4900 5850 4400 5850
Connection ~ 4400 5850
Wire Wire Line
	4400 5850 4400 6000
$Comp
L fab:Conn_PinHeader_1x01_P2.54mm_Horizontal_SMD J2
U 1 1 60C141E5
P 3650 3800
F 0 "J2" V 3804 3712 50  0000 R CNN
F 1 "Conn_PinHeader_1x01_P2.54mm_Horizontal_SMD" V 3713 3712 50  0000 R CNN
F 2 "fab:PinHeader_1x01_P2.54mm_Horizontal_SMD" H 3650 3800 50  0001 C CNN
F 3 "~" H 3650 3800 50  0001 C CNN
	1    3650 3800
	0    -1   -1   0   
$EndComp
$Comp
L fab:Microcontroller_ATtiny412_SSFR U1
U 1 1 60ACE5BA
P 2750 3450
F 0 "U1" H 2220 3496 50  0000 R CNN
F 1 "Microcontroller_ATtiny412_SSFR" H 2220 3405 50  0000 R CNN
F 2 "fab:SOIC-8_3.9x4.9mm_P1.27mm" H 2750 3450 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 2750 3450 50  0001 C CNN
	1    2750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3450 3650 3450
Wire Wire Line
	3650 3450 3650 3600
Wire Wire Line
	6250 3200 6250 3850
Wire Wire Line
	6250 4150 6250 4800
Wire Wire Line
	4950 3600 4950 4350
$EndSCHEMATC
