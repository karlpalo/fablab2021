EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Conn_01x09_Male J2
U 1 1 605FB504
P 6150 3050
F 0 "J2" H 6258 3631 50  0000 C CNN
F 1 "header_pins_side2" H 6258 3540 50  0000 C CNN
F 2 "fab:Header_SMD_01x09_P2.54mm_Horizontal_Male" H 6150 3050 50  0001 C CNN
F 3 "~" H 6150 3050 50  0001 C CNN
	1    6150 3050
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x06_Male Serial_port_pins1
U 1 1 605FC4C4
P 4550 1400
F 0 "Serial_port_pins1" H 4658 1781 50  0000 C CNN
F 1 "Conn_01x06_Male" H 4658 1690 50  0000 C CNN
F 2 "fab:Header_SMD_01x06_P2.54mm_Horizontal_Male" H 4550 1400 50  0001 C CNN
F 3 "~" H 4550 1400 50  0001 C CNN
	1    4550 1400
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_01x03_Male UPDI_programming_pins1
U 1 1 605FDA44
P 6550 1400
F 0 "UPDI_programming_pins1" H 6658 1681 50  0000 C CNN
F 1 "Conn_01x03_Male" H 6658 1590 50  0000 C CNN
F 2 "fab:Header_SMD_01x03_P2.54mm_Horizontal_Male" H 6550 1400 50  0001 C CNN
F 3 "~" H 6550 1400 50  0001 C CNN
	1    6550 1400
	1    0    0    -1  
$EndComp
$Comp
L fab:C C2
U 1 1 605FEE17
P 2800 6200
F 0 "C2" H 2915 6246 50  0000 L CNN
F 1 "10uF Output Cap for regulator and power supply filtering" H 2915 6155 50  0000 L CNN
F 2 "fab:C_1206" H 2838 6050 50  0001 C CNN
F 3 "" H 2800 6200 50  0001 C CNN
	1    2800 6200
	1    0    0    -1  
$EndComp
$Comp
L fab:C C3
U 1 1 605FF4AC
P 1400 6200
F 0 "C3" H 1515 6246 50  0000 L CNN
F 1 "10uF input cap for regulator" H 1515 6155 50  0000 L CNN
F 2 "fab:C_1206" H 1438 6050 50  0001 C CNN
F 3 "" H 1400 6200 50  0001 C CNN
	1    1400 6200
	1    0    0    -1  
$EndComp
$Comp
L fab:R R1
U 1 1 60600184
P 7600 1500
F 0 "R1" H 7670 1546 50  0000 L CNN
F 1 "4.7k resistor" H 7670 1455 50  0000 L CNN
F 2 "fab:R_1206" V 7530 1500 50  0001 C CNN
F 3 "~" H 7600 1500 50  0001 C CNN
	1    7600 1500
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R3
U 1 1 60600A7A
P 3300 3850
F 0 "R3" H 3370 3896 50  0000 L CNN
F 1 "1k resistor - Ballast for led" H 3370 3805 50  0000 L CNN
F 2 "fab:R_1206" V 3230 3850 50  0001 C CNN
F 3 "~" H 3300 3850 50  0001 C CNN
	1    3300 3850
	1    0    0    -1  
$EndComp
$Comp
L fab:R F1
U 1 1 60601088
P 1100 5700
F 0 "F1" H 1170 5746 50  0000 L CNN
F 1 "Potential place for fuse, otherwise use jumper" H 1170 5655 50  0000 L CNN
F 2 "fab:R_1206" V 1030 5700 50  0001 C CNN
F 3 "~" H 1100 5700 50  0001 C CNN
	1    1100 5700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 605F8430
P 1950 1050
F 0 "#PWR0101" H 1950 800 50  0001 C CNN
F 1 "GND" H 1955 877 50  0000 C CNN
F 2 "" H 1950 1050 50  0001 C CNN
F 3 "" H 1950 1050 50  0001 C CNN
	1    1950 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5850 1400 5850
Wire Wire Line
	1400 5850 1400 6050
Wire Wire Line
	1400 5850 1850 5850
Connection ~ 1400 5850
Wire Wire Line
	1400 6350 1400 6700
Wire Wire Line
	2450 5850 2800 5850
Wire Wire Line
	2800 5850 2800 6050
Wire Wire Line
	2800 6350 2800 6650
Text GLabel 1400 6700 0    50   Input ~ 0
GND
Text GLabel 2150 6650 0    50   Input ~ 0
GND
Text GLabel 2800 6650 0    50   Input ~ 0
GND
Text GLabel 1900 1000 0    50   Input ~ 0
GND
Wire Wire Line
	1900 1000 1950 1000
Wire Wire Line
	1950 1000 1950 1050
Wire Wire Line
	2800 5850 3350 5850
Text GLabel 3350 5850 0    50   Input ~ 0
REG5V
Connection ~ 2800 5850
Text GLabel 2100 1850 0    50   Input ~ 0
REG5V
$Comp
L fab:C C1
U 1 1 605FE739
P 2950 1000
F 0 "C1" H 3065 1046 50  0000 L CNN
F 1 "0.1uF -decoupling" H 3065 955 50  0000 L CNN
F 2 "fab:C_1206" H 2988 850 50  0001 C CNN
F 3 "" H 2950 1000 50  0001 C CNN
	1    2950 1000
	1    0    0    -1  
$EndComp
$Comp
L fab:Microcontroller_ATtiny1614-SSFR U1
U 1 1 605F3821
P 2500 3050
F 0 "U1" H 2500 3931 50  0000 C CNN
F 1 "Microcontroller_ATtiny1614-SSFR" H 2500 3840 50  0000 C CNN
F 2 "fab:SOIC-14_3.9x8.7mm_P1.27mm" H 2500 3050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf" H 2500 3050 50  0001 C CNN
	1    2500 3050
	1    0    0    -1  
$EndComp
Text GLabel 5650 4450 2    50   Input ~ 0
REG5V
Wire Wire Line
	2100 1850 2500 1850
Wire Wire Line
	2500 1850 2500 2350
Wire Wire Line
	5650 4450 5150 4450
Wire Wire Line
	6350 2850 6350 2750
Connection ~ 6350 2750
Wire Wire Line
	6350 2750 6350 2650
Wire Wire Line
	6350 2750 6900 2750
Text GLabel 6900 2750 2    50   Input ~ 0
GND
Wire Wire Line
	5150 4050 5500 4050
Wire Wire Line
	5150 4150 5500 4150
Wire Wire Line
	5150 4250 5500 4250
Wire Wire Line
	3100 3150 3450 3150
Wire Wire Line
	3100 3250 3450 3250
Wire Wire Line
	3100 3350 3450 3350
Text GLabel 3450 3150 2    50   Input ~ 0
PA4_connect
Text GLabel 5500 4250 2    50   Input ~ 0
PA4_connect
Text GLabel 5500 4150 2    50   Input ~ 0
PA5_connect
Text GLabel 3450 3250 2    50   Input ~ 0
PA5_connect
Text GLabel 5500 4050 2    50   Input ~ 0
PA6_connect
Text GLabel 3450 3350 2    50   Input ~ 0
PA6_connect
Wire Wire Line
	3100 3450 3300 3450
Wire Wire Line
	3300 3450 3300 3700
$Comp
L fab:LED LED1
U 1 1 60602249
P 3450 4250
F 0 "LED1" H 3443 4466 50  0000 C CNN
F 1 "LED - connect to PA7, arduino pin 3" H 3443 4375 50  0000 C CNN
F 2 "fab:LED_1206" H 3450 4250 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 3450 4250 50  0001 C CNN
	1    3450 4250
	-1   0    0    1   
$EndComp
Text GLabel 4450 4250 2    50   Input ~ 0
GND
Wire Wire Line
	4450 4250 3600 4250
Wire Wire Line
	3300 4000 3300 4250
Wire Wire Line
	1900 2950 1400 2950
Wire Wire Line
	1900 3050 1400 3050
Text GLabel 5500 3850 2    50   Input ~ 0
TXD_connect
Text GLabel 1400 2950 0    50   Input ~ 0
TXD_connect
Wire Wire Line
	4750 1600 5100 1600
Text GLabel 5100 1600 2    50   Input ~ 0
TXD_connect
Text GLabel 5500 3750 2    50   Input ~ 0
RXD_connect
Text GLabel 5100 1500 2    50   Input ~ 0
RXD_connect
Text GLabel 1400 3050 0    50   Input ~ 0
RXD_connect
Wire Wire Line
	5100 1500 4750 1500
Wire Wire Line
	4750 1400 5100 1400
Wire Wire Line
	4750 1200 5100 1200
Text GLabel 5100 1400 2    50   Input ~ 0
REG5V
Text GLabel 5100 1200 2    50   Input ~ 0
GND
Wire Wire Line
	6750 1400 7450 1400
Wire Wire Line
	6750 1500 7450 1500
Text GLabel 7450 1400 2    50   Input ~ 0
GND
Text GLabel 7650 1300 2    50   Input ~ 0
REG5V
Wire Wire Line
	6750 1300 7650 1300
Wire Wire Line
	7750 1500 8100 1500
Text GLabel 8100 1500 2    50   Input ~ 0
UPDI_RST
Wire Wire Line
	6350 3250 6900 3250
Text GLabel 6900 3250 2    50   Input ~ 0
UPDI_RST
Text GLabel 3450 2750 2    50   Input ~ 0
UPDI_RST
Wire Wire Line
	1900 2750 1400 2750
Wire Wire Line
	3100 2750 3450 2750
Wire Wire Line
	3100 2850 3450 2850
Wire Wire Line
	3100 2950 3450 2950
Wire Wire Line
	3100 3050 3450 3050
Wire Wire Line
	1900 2850 1400 2850
Wire Wire Line
	2950 650  2950 850 
Wire Wire Line
	2950 1150 2950 1400
Text GLabel 2950 650  0    50   Input ~ 0
REG5V
Text GLabel 2950 1400 0    50   Input ~ 0
GND
Text GLabel 3450 2850 2    50   Input ~ 0
PA1_connect
Text GLabel 3450 2950 2    50   Input ~ 0
PA2_connect
Text GLabel 3450 3050 2    50   Input ~ 0
PA3_connect
Text GLabel 1400 2850 0    50   Input ~ 0
PB0_connect
Text GLabel 1400 2750 0    50   Input ~ 0
PB1_connect
Text GLabel 6350 2950 2    50   Input ~ 0
PA3_connect
Text GLabel 6350 3050 2    50   Input ~ 0
PA2_connect
Text GLabel 6350 3150 2    50   Input ~ 0
PA1_connect
Text GLabel 6350 3350 2    50   Input ~ 0
PB0_connect
Text GLabel 6350 3450 2    50   Input ~ 0
PB1_connect
Wire Wire Line
	1100 5550 1100 5150
Wire Wire Line
	1100 5150 950  5150
Wire Wire Line
	4750 1300 5100 1300
Wire Wire Line
	4750 1700 5100 1700
Wire Wire Line
	3300 3450 3450 3450
Connection ~ 3300 3450
Text GLabel 5500 3950 2    50   Input ~ 0
Led_Connect
Text GLabel 3450 3450 2    50   Input ~ 0
Led_Connect
$Comp
L power:+5V #PWR0102
U 1 1 605FA4B8
P 950 5150
F 0 "#PWR0102" H 950 5000 50  0001 C CNN
F 1 "+5V" H 965 5323 50  0000 C CNN
F 2 "" H 950 5150 50  0001 C CNN
F 3 "" H 950 5150 50  0001 C CNN
	1    950  5150
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6067FC12
P 1850 5250
F 0 "#FLG0101" H 1850 5325 50  0001 C CNN
F 1 "PWR_FLAG" H 1850 5423 50  0000 C CNN
F 2 "" H 1850 5250 50  0001 C CNN
F 3 "~" H 1850 5250 50  0001 C CNN
	1    1850 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 6150 2150 6650
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6068774D
P 700 5150
F 0 "#FLG0102" H 700 5225 50  0001 C CNN
F 1 "PWR_FLAG" H 700 5323 50  0000 C CNN
F 2 "" H 700 5150 50  0001 C CNN
F 3 "~" H 700 5150 50  0001 C CNN
	1    700  5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  5150 950  5150
Connection ~ 950  5150
Wire Wire Line
	1850 5250 1850 5850
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 6068CDA7
P 1900 3750
F 0 "#FLG0103" H 1900 3825 50  0001 C CNN
F 1 "PWR_FLAG" H 1900 3923 50  0000 C CNN
F 2 "" H 1900 3750 50  0001 C CNN
F 3 "~" H 1900 3750 50  0001 C CNN
	1    1900 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1900 3750 2500 3750
Wire Wire Line
	2500 3750 2500 4000
Connection ~ 2500 3750
Text GLabel 2500 4000 0    50   Input ~ 0
GND
NoConn ~ 5100 1700
NoConn ~ 5100 1300
NoConn ~ 12150 3350
$Comp
L fab:Conn_01x02_Male P1
U 1 1 606A6CD1
P 700 4450
F 0 "P1" H 808 4631 50  0000 C CNN
F 1 "Power input" H 808 4540 50  0000 C CNN
F 2 "fab:Header_SMD_01x02_P2.54mm_Horizontal_Male" H 700 4450 50  0001 C CNN
F 3 "~" H 700 4450 50  0001 C CNN
	1    700  4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  4550 1250 4550
Wire Wire Line
	900  4450 1250 4450
Wire Wire Line
	1100 5150 1100 5050
Connection ~ 1100 5150
Text GLabel 1100 5050 1    50   Input ~ 0
PWR
Text GLabel 1250 4450 2    50   Input ~ 0
PWR
Text GLabel 1250 4550 2    50   Input ~ 0
GND
Wire Wire Line
	5150 3850 5500 3850
Wire Wire Line
	5150 3950 5500 3950
Wire Wire Line
	5150 3750 5500 3750
Wire Wire Line
	5150 4350 5150 4450
Connection ~ 5150 4450
Wire Wire Line
	5150 4450 5150 4550
$Comp
L fab:Conn_01x09_Male J1
U 1 1 605FA8A2
P 4950 4150
F 0 "J1" H 5058 4731 50  0000 C CNN
F 1 "Header_pins_side1" H 5058 4640 50  0000 C CNN
F 2 "fab:Header_SMD_01x09_P2.54mm_Horizontal_Male" H 4950 4150 50  0001 C CNN
F 3 "~" H 4950 4150 50  0001 C CNN
	1    4950 4150
	1    0    0    -1  
$EndComp
Connection ~ 1850 5850
$Comp
L fab:Regulator_Linear_NCP1117-5.0V-1A U2
U 1 1 606033C8
P 2150 5850
F 0 "U2" H 2150 6092 50  0000 C CNN
F 1 "Regulator_Linear_NCP1117-5.0V-1A" H 2150 6001 50  0000 C CNN
F 2 "fab:SOT-223-3_TabPin2" H 2150 6050 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NCP1117-D.PDF" H 2250 5600 50  0001 C CNN
	1    2150 5850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
