+++
title = "Assignment 06 - Electronics Design"
description = "Electronics Design"
+++

# Apologies and caveats
This weeks project is related to my final project, as I will be making a chip that would be able to operate a burn wire. In effect, it will be a 9V battery powering a attiny 412 chip through a 3.33V regululator, with the attiny being able to trigger the full 9V voltage to an electrical outlet that will operate the burn wire using a N-type mosfet to control the current.

## Getting started
First I installed [KiCAD](https://kicad.org/) on my computer.
Then I used git to get the FABLAB-kicad repository from https://gitlab.fabcloud.org/pub/libraries/electronics/kicad (and read trough the readme.md to set up the library to work with KiCAD!)

To understand a little bit more about the attiny processor, I took a look at its [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf).
And extended the pin diagram one could find at the [MegaTinyCore-git](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.gif)
{{< image src="attiny_pin_diagram.jpg" alt="attiny pin diagram">}}{{</image>}}

## KiCAD

In kicad I drew the board design, I was mostly able to proceed on my own, as my board had a fairly simple structure. However, due to my limited understanding of electronics, I ended up having to make 3 versions of the board.
<ul>
<li>The first one had the Mosfet source (e.g. the ground) and the drain (e.g. the 9V voltage pin), installed the wrong way around. The second one </li> 
</ul>
