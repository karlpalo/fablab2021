+++
title = "About Page"
description = "This is about page"
+++


{{< imagetoside src="me.jpg" alt="A fancy artsy pic of me">}}

#Hello!

I am Karl and this is my Fablab project website.

I am an almost 30-year old, working on my Ph.D. studies here at Aalto University. My focus area is cellulose chemistry, and my thesis topic is making nanocellulose out of algae.

As far as fabricating things goes, I am a bit of a tinkerer. I have passable familiarity with basic woodworking, some experience from fine tinkering due to my hobby of building miniatures, and the latest effort has been the Prusa MK3s FDM printer I bought for myself last autumn.


{{</imagetoside>}}
